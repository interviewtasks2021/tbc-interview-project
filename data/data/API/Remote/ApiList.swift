//
//  ApiList.swift
//  data
//
//  Created by Rashad Shirizada on 03.11.21.
//

import Foundation

enum RemoteAPI : ApiProtocol {
    static let baseUrl = "https://api.themoviedb.org/3"
    static let imageUrl = "http://image.tmdb.org/t/p/w500"
    private static let apiKey = "1d59e05c8236976b82dd7149ddea819c" // you can change to yours
    case getSeries(queryItems: [URLQueryItem], type: SerieTypeDto)
    
    var urlStr: String {
        switch self {
        case .getSeries(_, let type):
            switch type {
            case .popular:
                return "/movie/popular"
            case .similar(let movieId):
                return "/movie/\(movieId)/similar"
            }
        }
    }
    
    var defaultUrlQueryItems: [URLQueryItem] {
        var items : [URLQueryItem] = []
        items.append(URLQueryItem(name: "api_key", value: RemoteAPI.apiKey))
        items.append(URLQueryItem(name: "language", value: "en-US"))
        return items
    }
    
    var url: URL {
        var totalQueryItems = defaultUrlQueryItems
        var url = URL(string: (RemoteAPI.baseUrl +  urlStr))!
        switch self {
        case .getSeries(let queryItems, _ ):
            totalQueryItems.append(contentsOf: queryItems)
        }
        
        url.setQueryItems(queryItems: totalQueryItems)
        return url
    }
}
