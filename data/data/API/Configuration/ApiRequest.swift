//
//  ApiRequestProtocol.swift
//  data
//
//  Created by Rashad Shirizada on 03.11.21.
//

import Foundation

struct ApiRequest : ApiRequestProtocol {
    var api: ApiProtocol
    var method: ApiRequestMethod
    var bodyType: RequestBodyType
    var body: [String:Any]?
    var headers: [String:String]?
    
    init(api: ApiProtocol,
         method: ApiRequestMethod,
         bodyType: RequestBodyType = .json,
         body: [String:Any]? = nil ,
         headers: [String:String]?  = nil) {
        self.api = api
        self.method = method
        self.body = body
        self.bodyType = bodyType
        self.headers = headers
    }
}
