//
//  ApiRequestProtocol.swift
//  data
//
//  Created by Rashad Shirizada on 03.11.21.
//

import Foundation
protocol ApiRequestProtocol  {
    var api: ApiProtocol {get}
    var method: ApiRequestMethod {get}
    var bodyType: RequestBodyType {get}
    var body: [String:Any]? {get}
    var headers: [String:String]? { get}
    var request:URLRequest {get}
}

extension ApiRequestProtocol {
    var request: URLRequest {
        var request = URLRequest(url: api.url)
        request.httpMethod = method.rawValue
        
        if let body = body {
            let data = try? JSONSerialization.data(withJSONObject: body, options: [])
            request.httpBody = data
        }
        request.allHTTPHeaderFields = totalHeaders
        
        return request
    }
    
    var totalHeaders: [String:String] {
        var totalHeaders: [String:String] = [:]
        
        if let headers = headers {
            headers.forEach { key,value in
                totalHeaders.updateValue(value, forKey: key)
            }
        }
        
        self.bodyType.header.forEach { key,value in
            totalHeaders.updateValue(value, forKey: key)
        }
        
        return totalHeaders
    }

    
}
