//
//  ApiRequestMethod.swift
//  data
//
//  Created by Rashad Shirizada on 03.11.21.
//

import Foundation

enum ApiRequestMethod: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
    case delete = "DELETE"
}
