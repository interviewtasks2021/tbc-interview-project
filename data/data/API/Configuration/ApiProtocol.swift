//
//  ApiProtocol.swift
//  data
//
//  Created by Rashad Shirizada on 03.11.21.
//

import Foundation

protocol ApiProtocol {
    var urlStr: String { get }
    var url: URL {get}
}
