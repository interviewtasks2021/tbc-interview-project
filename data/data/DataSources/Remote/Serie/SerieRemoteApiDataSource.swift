//
//  SerieRemoteApiDataSource.swift
//  data
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import RxSwift
class SerieRemoteApiDataSource: SerieRemoteDataSourceProtocol {
    
    let apiManager: ApiManager
    
    init(apiManager: ApiManager) {
        self.apiManager = apiManager
    }
    
    func fetchSeries(paging: PagingDto,type:SerieTypeDto) -> Single<PaginatedDto<SerieDto>> {
        let api : RemoteAPI = .getSeries(queryItems: paging.queryItems, type: type)
        let apiRequest = ApiRequest(api: api, method: .get)
        return apiManager.fetch(apiRequest: apiRequest)
    }
}
