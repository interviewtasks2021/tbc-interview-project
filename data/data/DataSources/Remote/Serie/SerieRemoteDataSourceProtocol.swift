//
//  SerieRemoteApiDataSourceProtocol.swift
//  data
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import RxSwift

protocol SerieRemoteDataSourceProtocol {
    func fetchSeries(paging: PagingDto, type: SerieTypeDto) -> Single<PaginatedDto<SerieDto>>
}
