//
//  SerieRemoteMockDataSource.swift
//  data
//
//  Created by Rashad Shirizada on 08.11.21.
//

import Foundation
import RxSwift
class SerieRemoteMockDataSource: SerieRemoteDataSourceProtocol {
    static let shared = SerieRemoteMockDataSource()
    private init(){
        
    }
    
    func fetchSeries(paging: PagingDto, type: SerieTypeDto) -> Single<PaginatedDto<SerieDto>> {
        return FileReader.shared.readJsonFromLocal(jsonFileName: "series.json")
    }
}
