//
//  SerieRepository.swift
//  data
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import RxSwift
import domain

class SerieRepository: SerieRepositoryProtocol {
    
    let remoteDataSource: SerieRemoteDataSourceProtocol
    
    init(remoteDataSource: SerieRemoteDataSourceProtocol) {
        self.remoteDataSource = remoteDataSource
    }
    
    func getSeriesList(paging: PagingEntity, type: SerieTypeEntity) -> Observable<PaginatedEntity<SerieEntity>> {
        return remoteDataSource.fetchSeries(paging: paging.map(), type: type.map()).asObservable().map({$0.map()})
    }
}
