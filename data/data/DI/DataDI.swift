//
//  DataDI.swift
//  data
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import Swinject

public class DataAssembly{
    public static var assemblies: [Assembly]{
        return [
            RepositoryProtocolsDI(),
            RemoteDataSourceDI()
        ]
    }
}
