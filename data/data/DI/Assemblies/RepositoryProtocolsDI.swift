//
//  RepositoryProtocolsDI.swift
//  data
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import Swinject
import domain

public class RepositoryProtocolsDI : Assembly{
   
    public init() {
    }
    
    public func assemble(container: Container) {
        container.register(SerieRepositoryProtocol.self) { r in
            let remoteDataSource = r.resolve(SerieRemoteDataSourceProtocol.self)!
            return SerieRepository(remoteDataSource: remoteDataSource)
        }
    }
}
