//
//  RemoteDataSourceDI.swift
//  data
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import Foundation
import Swinject

public class RemoteDataSourceDI : Assembly{
    
    public init() {
    }
    
    public func assemble(container: Container) {
        container.register(SerieRemoteDataSourceProtocol.self) { r in
            return SerieRemoteApiDataSource(apiManager: .shared)
        }
    }
}
