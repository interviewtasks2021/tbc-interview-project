//
//  Mapper.swift
//  data
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation

protocol Mapper {
 
    associatedtype output
    
    func map() -> output
    
}
