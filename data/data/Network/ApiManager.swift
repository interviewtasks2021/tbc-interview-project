
import Foundation
import RxSwift

class ApiManager {
    
    static let shared = ApiManager()
    private init() {
        
    }
    
    func fetch<T: Codable> (apiRequest: ApiRequestProtocol) -> Single<T> {
        
        return Single.create { single in
            
            let session = URLSession.shared
            let request = apiRequest.request
            session.dataTask(with: request) { data, response, error in
                debugPrint(request)
                if let error = error {
                    single(.failure(error))
                }
                else {
                    ApiResponseHandler.shared.handle(single: single, api: apiRequest, response: response, data: data)
                }
            }.resume()
            
            return Disposables.create()
        }
    }
}
