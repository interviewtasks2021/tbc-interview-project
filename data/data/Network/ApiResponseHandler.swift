
import Foundation
import RxSwift
class ApiResponseHandler {
        static let shared = ApiResponseHandler()
        private init() {
    
        }
    
     func handle<T: Codable> (single: ((SingleEvent<T>) -> Void),
                                    api: ApiRequestProtocol,
                                    response: URLResponse?,
                                    data: Data?) {
        guard let response = response as? HTTPURLResponse else{
            
            single(.failure(ApiManagerError.notHttpUrlResponse))
            return
        }
        let statusCode = response.statusCode
        
        switch statusCode {
        case 200,201:
            guard let data = data else {
                return
            }
            do {
                let model = try JSONDecoder().decode(T.self, from: data)
                single(.success(model))
            }
            catch {
                single(.failure(error))
            }
        case 400:
            single(.failure(ApiManagerError.badRequest))
        case 401:
            single(.failure(ApiManagerError.unauthorized))
        case 404:
            single(.failure(ApiManagerError.notFound))
        default:
            single(.failure(ApiManagerError.unhandledError(statusCode: statusCode)))
        }
    }
}
