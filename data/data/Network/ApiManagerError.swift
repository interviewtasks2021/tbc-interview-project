
import Foundation

enum ApiManagerError : Error {
    case notHttpUrlResponse
    case unauthorized
    case notFound
    case badRequest
    case unhandledError(statusCode: Int)
}
