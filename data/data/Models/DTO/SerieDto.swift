//
//  SerieDto.swift
//  data
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import domain

struct SerieDto: Codable {
    var id: Int
    var title: String
    var averageRating: Double
    var photo: URL?
    var desc:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "original_title"
        case averageRating = "vote_average"
        case photo = "poster_path"
        case desc = "overview"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(Int.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.averageRating = try container.decode(Double.self, forKey: .averageRating)
        if let photoPath = try? container.decode(String.self, forKey: .photo) {
            self.photo = URL(string: RemoteAPI.imageUrl + photoPath)
        }
        self.desc = try? container.decode(String.self, forKey: .desc)
    }
}


extension SerieDto : Mapper {
    typealias output = SerieEntity
    
    func map() -> output {
        return SerieEntity(id: id,
                           title: title,
                           averageRating: averageRating,
                           photo: photo,
                           desc: desc)
    }
}
