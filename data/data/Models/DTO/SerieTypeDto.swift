//
//  SerieTypeDto.swift
//  data
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation

enum SerieTypeDto {
    case popular
    case similar(movieId: Int)
}
