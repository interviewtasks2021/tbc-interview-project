//
//  PagingDto.swift
//  data
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import domain

struct PagingDto  {
    var page: Int
    var perPage: Int
}

