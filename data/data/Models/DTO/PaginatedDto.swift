//
//  PaginatedDto.swift
//  data
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import domain

struct PaginatedDto < T : Codable> : Codable where T : Mapper {
    var currentPage: Int
    var results: [T]
    var totalPages:Int
    
    enum CodingKeys : String, CodingKey{
        case currentPage = "page"
        case results = "results"
        case totalPages = "total_pages"
    }
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.currentPage = try container.decode(Int.self, forKey: .currentPage)
        self.results = try container.decode([T].self, forKey: .results)
        self.totalPages = try container.decode(Int.self, forKey: .totalPages)
    }
}

extension PaginatedDto : Mapper {
    typealias output = PaginatedEntity<T.output>
    
    func map() -> output {
        let hasNext = currentPage < totalPages
        return PaginatedEntity(hasNext: hasNext, items: results.map({$0.map()}))
    }
}
