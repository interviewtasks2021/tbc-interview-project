//
//  PagingDto + Extensions.swift
//  data
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
extension PagingDto {
    var queryItems: [URLQueryItem]  {
        return [
        URLQueryItem(name: "page", value: String(page)),
        URLQueryItem(name: "per_page", value: String(perPage))
        ]
    }
}
