//
//  PagingEntity + Extensions.swift
//  data
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import domain

extension PagingEntity : Mapper  {
    typealias output = PagingDto
    
    func map() -> PagingDto {
        return PagingDto(page: pageNum, perPage: perPage)
    }
}
