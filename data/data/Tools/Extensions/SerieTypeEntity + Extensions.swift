//
//  SerieTypeEntity + Extensions.swift
//  data
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import domain

extension SerieTypeEntity : Mapper {
    typealias output =  SerieTypeDto
    
    func map() -> SerieTypeDto { // should think about enum mappers
        switch self {
        case .popular:
            return .popular
        case .similar(movideID: let id):
            return .similar(movieId: id)
        }
    }
}
