import Foundation
import RxSwift


enum FileReaderError:Error{
    case pathNotFound
}

class FileReader {
    static let shared = FileReader()
    
    func readJsonFromLocal<T:Codable>(jsonFileName:String)->Single<T> {
        return  Single.create { (single) -> Disposable in
            do{
                guard let path =  Bundle.main.path(forResource: jsonFileName, ofType: "json") else{
                    single(.failure(FileReaderError.pathNotFound))
                    return Disposables.create()
                }
                
                let jsonData = try  NSData(contentsOfFile: path, options: .mappedRead)
                let decoder = JSONDecoder()
                let value = try decoder.decode(T.self, from: jsonData as Data)
                
                single(.success(value))
                return Disposables.create()
                
            }
            catch{
                single(.failure(error))
                return Disposables.create()
            }
        }
    }
}
