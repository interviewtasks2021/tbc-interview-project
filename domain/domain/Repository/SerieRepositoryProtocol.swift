//
//  SeriesRepositoryProtocol.swift
//  domain
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import RxSwift

public protocol SerieRepositoryProtocol {
    func getSeriesList(paging: PagingEntity, type: SerieTypeEntity) -> Observable<PaginatedEntity<SerieEntity>>
}
