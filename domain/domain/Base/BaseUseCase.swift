//
//  BaseUseCase.swift
//  domain
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
protocol BaseUseCase {
    associatedtype InputType
    
    associatedtype OutputType
    
    func execute(input: InputType) -> OutputType
}
