//
//  GetSeriesUseCase.swift
//  domain
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import RxSwift

public class GetSeriesUseCase  : BaseUseCase{
    typealias InputType = (PagingEntity, SerieTypeEntity)
    typealias OutputType = Observable<PaginatedEntity<SerieEntity>>
    
    let seriesRepository: SerieRepositoryProtocol
    
    init(seriesRepository: SerieRepositoryProtocol) {
        self.seriesRepository = seriesRepository
    }
    
  public  func execute(input: (PagingEntity,SerieTypeEntity)) -> Observable<PaginatedEntity<SerieEntity>> {
      return seriesRepository.getSeriesList(paging: input.0,type: input.1)
    }
}
