//
//  UseCasesDI.swift
//  domain
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import Swinject

public class UseCasesDI : Assembly{
    public init() {
    }
    
    public func assemble(container: Container) {
        container.register(GetSeriesUseCase.self) { r in
            let seriesRepo = r.resolve(SerieRepositoryProtocol.self)!
            return GetSeriesUseCase(seriesRepository:seriesRepo )
        }
    }
}
