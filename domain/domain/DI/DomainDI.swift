//
//  DomainDI.swift
//  domain
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import Swinject

public class DomainAseembly {
    
    public static var assemblies: [Assembly]{
        return [
            UseCasesDI()
        ]
    }
}

