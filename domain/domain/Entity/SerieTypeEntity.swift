//
//  SerieTypeEntity.swift
//  domain
//
//  Created by Rashad Shirizada on 08.11.21.
//

import Foundation
public enum SerieTypeEntity {
    case popular
    case similar(movideID: Int)
}
