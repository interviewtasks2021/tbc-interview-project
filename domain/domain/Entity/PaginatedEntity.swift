//
//  PaginatedEntity.swift
//  domain
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation

public struct PaginatedEntity<T> {
    public var hasNext: Bool
    public var items: [T]
    
    public init(hasNext: Bool, items: [T]) {
        self.hasNext = hasNext
        self.items = items
    }
}
