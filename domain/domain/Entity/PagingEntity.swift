//
//  File.swift
//  domain
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
public struct PagingEntity {
    public var pageNum: Int
    public var perPage: Int
    
    public init(pageNum: Int, perPage: Int) {
        self.pageNum = pageNum
        self.perPage = perPage
    }
    
    public mutating func increasePage() {
        pageNum += 1
    }
 }
