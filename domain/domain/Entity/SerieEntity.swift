//
//  SerieEntity.swift
//  domain
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation

public struct SerieEntity {
    public var id: Int
    public var title:String
    public var averageRating: Double
    public var photo: URL?
    public var desc: String?

    public init(id: Int, title:String, averageRating: Double, photo: URL?, desc: String?) {
        self.id = id
        self.title = title
        self.averageRating = averageRating
        self.photo = photo
        self.desc = desc
    }
}


