//
//  ViewModelDI.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import Foundation
import Swinject
import domain

class ViewModelDI: Assembly{
    
    init(){
        
    }
    
    func assemble(container: Container){
        container.register(SeriesListViewModel.self) { (r, type: SerieTypeEntity) in
            let getSeriesUseCase = r.resolve(GetSeriesUseCase.self)!
           return SeriesListViewModel(getSeriesUseCase: getSeriesUseCase,type: type)
        }
        
        container.register(SerieDetailViewModel.self) { (r, model: SerieEntity, type: SerieTypeEntity) in
            let seriesListVm  = r.resolve(SeriesListViewModel.self,argument: type)!
           return SerieDetailViewModel(serie: model, seriesListViewModel: seriesListVm)
        }
    }
}
