//
//  ControllersDI.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import Foundation
import Foundation
import Swinject
import domain

class ControllersDI: Assembly{
    
    init(){
        
    }
    
    func assemble(container: Container){
        container.register(ControllerProviderProtocol.self) { r in
            ControllerProvider(resolver: r)
        }
 
    }
}
