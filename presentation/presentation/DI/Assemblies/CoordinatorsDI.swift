//
//  CoordinatorsDI.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import Foundation
import Foundation
import Swinject
import domain
import UIKit

class CoordinatorsDI: Assembly{
    
    init(){
    }
    
    func assemble(container: Container){
        
        container.register(AppCoordinator.self) { (r, window: UIWindow) -> AppCoordinator in
            let provider = r.resolve(ControllerProviderProtocol.self)!
            return AppCoordinator(window: window, provider: provider)
        }
        
        container.register(SeriesCoordinator.self) { (r, navigation: BaseNavigationController) -> SeriesCoordinator in
            let provider = r.resolve(ControllerProviderProtocol.self)!
            return SeriesCoordinator(navigationController: navigation,
                                         controllerProvider: provider )
        }
    }
}
