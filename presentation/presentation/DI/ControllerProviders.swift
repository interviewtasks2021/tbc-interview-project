//
//  ControllerProviders.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import Swinject
import domain
protocol ControllerProviderProtocol {
    func seriesCoordinator(navigation: BaseNavigationController) -> SeriesCoordinator
    
    func seriesListController(type: SerieTypeEntity) -> SeriesListController
    func serieDetailsController(serie: SerieEntity, type: SerieTypeEntity) -> SerieDetailsController
}

class ControllerProvider: ControllerProviderProtocol {
    let resolver: Resolver
    
    public init(resolver: Resolver){
        self.resolver = resolver
    }
    
    func seriesCoordinator(navigation: BaseNavigationController)  -> SeriesCoordinator {
        return resolver.resolve(SeriesCoordinator.self,argument:navigation)!
    }
    
    func seriesListController(type: SerieTypeEntity) -> SeriesListController{
        let vm = resolver.resolve(SeriesListViewModel.self,argument: type)!
        return SeriesListController(seriesListViewModel: vm)
    }
    
    func serieDetailsController(serie: SerieEntity, type: SerieTypeEntity) -> SerieDetailsController{
        let vm = resolver.resolve(SerieDetailViewModel.self,
                                  arguments: serie,type)!
        return SerieDetailsController(viewModel: vm)
    }
}
