//
//  PresentationDI.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import Swinject
public class PresentationAssembly{
    
    public static var assemblies: [Assembly]{
        return [
            CoordinatorsDI(),
            ControllersDI(),
            ViewModelDI()
        ]
    }
    
}
