//
//  Observable + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import Foundation
import RxSwift

extension Observable {
    func handleError()->Observable{
        return  self.catch { (error) -> Observable<Element> in            
            print("HANDLE ERROR HERE ",error.localizedDescription)
            return self
        }
    }
}
