//
//  UIScroll + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import UIKit
import RxSwift
extension UIScrollView {
    
    func enableVerticalPagination (viewmodel: PaginatableViewModel) -> Disposable {
        var mutatingVm = viewmodel
        return  self.rx.didScroll.subscribe(onNext: { _ in
            if (((self.contentOffset.y + self.frame.size.height) >= self.contentSize.height )){
               
                if viewmodel.hasNext && !viewmodel.isCurrentlyFetchingData {
                    mutatingVm.paging.increasePage()
                    mutatingVm.loadMore()
                }
            
            }
        })
    }
}
