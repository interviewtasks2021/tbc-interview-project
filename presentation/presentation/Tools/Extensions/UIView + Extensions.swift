//
//  UIView + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import UIKit

extension UIView {
    
    func fixView( _ contentView: UIView, padding: UIEdgeInsets = .zero) {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: padding.left),
            contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -padding.right),
            contentView.topAnchor.constraint(equalTo: self.topAnchor,constant:  padding.top),
            contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -padding.bottom)
        ])
    }
    
}
