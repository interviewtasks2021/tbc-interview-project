//
//  UICollectionFlowLayout + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import UIKit

extension UICollectionViewFlowLayout {
    
    static var similarSerieHorizontalCellLayout: UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 100, height: 140)
        return layout
    }
}
