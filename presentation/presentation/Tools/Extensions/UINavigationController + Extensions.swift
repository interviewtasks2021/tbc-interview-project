//
//  UINavigationController + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import UIKit

extension UINavigationController {
    func hideNavigationBar() {
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
    }
}
