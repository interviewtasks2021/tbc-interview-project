//
//  UIColor + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import UIKit
//for these, we can just use SwiftGen library to make everything much easier
extension UIColor {
    static var bundle: Bundle? {
        return  Bundle(identifier: "com.rashad.tbc-test.presentation")
    }
    static let viewBackgroundColor = UIColor(named: "viewBackgroundColor", in: bundle, compatibleWith: nil)
    static let titleColor = UIColor(named: "titleColor", in: bundle, compatibleWith: nil)
    static let descColor = UIColor(named: "descColor", in: bundle, compatibleWith: nil)

}
