//
//  UICollectionView + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import UIKit

extension UICollectionView {
    func registerCell(cell: RegisterableCell.Type) {
        self.register(cell.nib, forCellWithReuseIdentifier: cell.reuseIdentifier)
    }
}
