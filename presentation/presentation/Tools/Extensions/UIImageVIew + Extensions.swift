//
//  UIImageVIew + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView{
    
    func loadImage(assetType: AssetType){
        switch assetType {
        case .localUI(image: let image):
            DispatchQueue.main.async {
                self.image = image
            }
        case .remote(let url):
            self.kf.setImage(with: url)
        }
    }
}

