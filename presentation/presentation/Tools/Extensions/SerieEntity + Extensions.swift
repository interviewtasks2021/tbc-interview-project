//
//  SerieEntity + Extensions.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//
import UIKit
import Foundation
import domain

extension SerieEntity {
    var assetType : AssetType {
        if let postPath = photo {
            return .remote(url: postPath)
        }
        else{
            return .localUI(image: UIImage(named: "some placeholder"))
        }
    }
}
