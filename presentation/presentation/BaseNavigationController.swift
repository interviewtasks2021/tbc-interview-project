//
//  BaseNavigationController.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import UIKit


public class BaseNavigationController: UINavigationController {
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hideNavigationBar()
    }
    
}
