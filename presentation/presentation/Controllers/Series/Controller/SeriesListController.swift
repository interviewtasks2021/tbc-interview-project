//
//  SeriesListController.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import UIKit
import RxSwift
import domain

class SeriesListController: BaseViewController {
    private lazy var searchBar: UISearchBar = {
        let searchbar = UISearchBar()
        searchbar.translatesAutoresizingMaskIntoConstraints = false
        searchbar.heightAnchor.constraint(equalToConstant: 48).isActive = true
        return searchbar
    }()
    
    private lazy var table: UITableView  = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.registerCell(cell: SerieCell.self)
        return table
    }()
    
    var disposeBag = DisposeBag()
    let seriesListViewModel: SeriesListViewModel
    
    var onItem: ((SerieEntity) -> ()) = { _ in }
    init(seriesListViewModel: SeriesListViewModel) {
        self.seriesListViewModel = seriesListViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleEvents()
        seriesListViewModel.loadData()
    }
    
    override func setupView() {
        super.setupView()
        addSearchbar()
        addTableView()
        setupTableView()
    }
    
    private func handleEvents() {
        self.table.rx.itemSelected.subscribe(onNext: { [ weak self] indexPath in
            guard let self = self,
                let serie = self.seriesListViewModel.getSerie(at: indexPath.row) else{return}
            self.onItem(serie)
        })
            .disposed(by: disposeBag)
        
        self.searchBar.rx.text.distinctUntilChanged()
            .debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { searchKey in
                print("HERE YOU CAN SEARCH, key = ", searchKey)
                //actually i couldnt find option in api to provide us with search functionality, thats why i left it here. main concept of search is so far ;)
            })
            .disposed(by: self.disposeBag)
    }
    
    private func addSearchbar() {
        view.addSubview(searchBar)
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 20),
            searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant:  -20)
        ])
    }
    
    private func addTableView() {
        view.addSubview(table)
        NSLayoutConstraint.activate([
            table.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 10),
            table.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            table.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            table.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func setupTableView() {
        self.table.enableVerticalPagination(viewmodel: seriesListViewModel).disposed(by: disposeBag)
        
        seriesListViewModel.serieSubjects.asObservable().map({$0 ?? []}).bind(to: self.table.rx.items(cellIdentifier: SerieCell.reuseIdentifier, cellType: SerieCell.self)) { row, data, cell in
            
            cell.configure(serie: data)
            
        }.disposed(by: disposeBag)
    }
}
