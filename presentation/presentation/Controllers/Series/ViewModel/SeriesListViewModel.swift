//
//  SeriesListViewModel.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import domain
import RxSwift

class SeriesListViewModel : PaginatableViewModel {
    var paging: PagingEntity = PagingEntity(pageNum: 1, perPage: 10) // initial
    var hasNext: Bool = true
    var isCurrentlyFetchingData: Bool = false
    var disposeBag = DisposeBag()
    let getSeriesUseCase: GetSeriesUseCase
    
    var serieSubjects: BehaviorSubject<[SerieEntity]?> = BehaviorSubject(value: nil)
    private var serieModels: [SerieEntity] {
        set {
            serieSubjects.onNext(newValue)
        }
        get {
            return (try? serieSubjects.value()) ?? []
        }
    }
    let type: SerieTypeEntity
    
    init(getSeriesUseCase : GetSeriesUseCase, type: SerieTypeEntity) {
        self.getSeriesUseCase = getSeriesUseCase
        self.type = type
    }
    
    func loadData() {
        self.isCurrentlyFetchingData = true
        getSeriesUseCase.execute(input: (paging,type)).asObservable()
            .handleError()
            .subscribe(onNext: { seriesEntity in
                self.hasNext = seriesEntity.hasNext
                self.serieModels.append(contentsOf: seriesEntity.items)
                self.isCurrentlyFetchingData = false
            })
            .disposed(by: disposeBag)
    }
    
    func loadMore() {
        loadData()
    }
    
    func getSerie( at index: Int) -> SerieEntity? {
        if index >= serieModels.count{
            return nil
        }
        return serieModels[index]
    }
}
