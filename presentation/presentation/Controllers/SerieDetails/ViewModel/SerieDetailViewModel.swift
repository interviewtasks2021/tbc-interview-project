//
//  SerieDetailViewModel.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import domain

class SerieDetailViewModel {
    
    let serieEntity: SerieEntity
    let seriesListViewModel: SeriesListViewModel
    
    init(serie: SerieEntity, seriesListViewModel: SeriesListViewModel) {
        self.serieEntity = serie
        self.seriesListViewModel = seriesListViewModel
    }
}
