//
//  SerieDetailsController.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import UIKit
import RxSwift

class SerieDetailsController: BaseScrollableController {
    
    private lazy var posterIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleToFill
        image.heightAnchor.constraint(equalToConstant: 200).isActive = true
        image.loadImage(assetType: viewModel.serieEntity.assetType)
        return image
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 20, weight: .bold)
        label.textColor = UIColor.titleColor
        label.textAlignment = .justified
        label.text = viewModel.serieEntity.title
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 16, weight: .regular)
//        label.textColor = UIColor.descc
        label.text = viewModel.serieEntity.desc
        return label
    }()
    
    private lazy var similarSeriesCollection : UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.similarSerieHorizontalCellLayout)
        collection.backgroundColor = .clear
        collection.registerCell(cell: SerieCollectionCell.self)
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    let viewModel: SerieDetailViewModel
    var disposeBag = DisposeBag()
    init(viewModel: SerieDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSimilarSeriesCollection()
        viewModel.seriesListViewModel.loadData()
    }
    
    override func setupView() {
        super.setupView()
        addImage()
        addTitle()
        addDescLabel()
        addSimilarSeriesCollection()
    }
    
    private func addImage () {
        contentView.addSubview(posterIcon)
        NSLayoutConstraint.activate([
            posterIcon.topAnchor.constraint(equalTo: contentView.topAnchor),
            posterIcon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            posterIcon.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
        
    }
    
    private func addTitle() {
        contentView.addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: posterIcon.bottomAnchor,constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -20)
        ])
    }
    
    private func addDescLabel() {
        contentView.addSubview(descLabel)
        NSLayoutConstraint.activate([
            descLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,constant: 20),
            descLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 20),
            descLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -20)
        ])
    }
    
    private func addSimilarSeriesCollection() {
        contentView.addSubview(similarSeriesCollection)
        NSLayoutConstraint.activate([
            similarSeriesCollection.topAnchor.constraint(greaterThanOrEqualTo: descLabel.bottomAnchor,constant: 20),
            similarSeriesCollection.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            similarSeriesCollection.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            similarSeriesCollection.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -40),
            similarSeriesCollection.heightAnchor.constraint(equalToConstant: 150)
        ])
    }
    
    private func setupSimilarSeriesCollection() {
        viewModel.seriesListViewModel.serieSubjects.map({$0 ?? []})
            .bind(to: self.similarSeriesCollection.rx.items(cellIdentifier: SerieCollectionCell.reuseIdentifier, cellType: SerieCollectionCell.self)) { row, data, cell in
                cell.configure(serie: data)
            }.disposed(by: disposeBag)
    }
}
