//
//  SerieCollectionCell.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import UIKit
import domain

class SerieCollectionCell : UICollectionViewCell, RegisterableCell {
    static var reuseIdentifier: String{
        return "sericollectioncell"
    }
    
    static var nibName: String {
        return "SerieCollectionCell"
    }
    
    @IBOutlet weak var posterIcon: UIImageView!
    @IBOutlet weak var posterTitle: UILabel!
    
    func configure(serie: SerieEntity) {
        posterIcon.loadImage(assetType: serie.assetType)
        posterTitle.text = serie.title
    }
}
