//
//  SerieCell.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import UIKit
import domain

class SerieCell : UITableViewCell, RegisterableCell {
    
    static var reuseIdentifier: String {
        return "seriecell"
    }
    static var nibName: String {
        return "SerieCell"
    }
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override  func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        selectionStyle = .none
        self.titleLabel.textColor = UIColor.titleColor
    }
    
    func configure(serie: SerieEntity) {
        self.icon.loadImage(assetType: serie.assetType)
        self.titleLabel.text = serie.title
        self.ratingLabel.text  = String(format: "average rating: %.1f", serie.averageRating)
    }
}
