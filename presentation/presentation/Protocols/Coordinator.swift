//
//  Coordinator.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
protocol Coordinator {
    var navigationController: BaseNavigationController {get}
    func start()
}
