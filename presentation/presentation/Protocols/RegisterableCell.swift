//
//  RegisterableCell.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import UIKit
protocol RegisterableCell {
    static var reuseIdentifier: String {get}
    static var nibName:String {get}
    static var nib:UINib {get}
}

extension RegisterableCell {
   static var nib: UINib {
       return UINib(nibName: nibName, bundle: Bundle.init(identifier: "com.rashad.tbc-test.presentation"))
    }
}
