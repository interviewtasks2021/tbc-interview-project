//
//  PaginatableViewModel.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import domain

protocol PaginatableViewModel {
    var paging: PagingEntity { set get}
    var hasNext: Bool { set get}
    var isCurrentlyFetchingData: Bool { set get}
    func loadMore()
}
