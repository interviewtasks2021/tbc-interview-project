//
//  BaseCoordinator.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import UIKit

public class BaseCoordiantor: Coordinator{
    var childCoordinator: [Coordinator] = []
    
    public var navigationController: BaseNavigationController
    internal  var controllerProvider: ControllerProviderProtocol
    
    init(navigationController: BaseNavigationController,
         controllerProvider: ControllerProviderProtocol){
        self.navigationController = navigationController
        self.controllerProvider = controllerProvider
    }
    
    func start() {
        
    }
    
}
