//
//  AssetType.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import Foundation
import Foundation
import UIKit

enum AssetType{
    case localUI(image: UIImage?)
    case remote(url: URL)
}
