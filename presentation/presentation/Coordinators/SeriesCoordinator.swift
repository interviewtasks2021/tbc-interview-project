//
//  SeriesCoordinator.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import UIKit
import domain

class SeriesCoordinator : BaseCoordiantor{

    override  func start() {
        showSerisList()
    }
    
    func showSerisList() {
        let seriesListController = controllerProvider.seriesListController(type: .popular)
        seriesListController.title = "Series"
        self.navigationController.pushViewController(seriesListController, animated: true)

        seriesListController.onItem = { serie in
            self.showSerieDetails(serieEntity: serie)
        }
    }
    
    func showSerieDetails(serieEntity : SerieEntity) {
        let type = SerieTypeEntity.similar(movideID: serieEntity.id)
        let serieDetailsController = controllerProvider.serieDetailsController(serie: serieEntity, type: type)
        self.navigationController.pushViewController(serieDetailsController, animated: true)
    }
    
}
