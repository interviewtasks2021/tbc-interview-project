//
//  AppCoordinator.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import Foundation
import UIKit

enum AppLaunchState {
    case firstTime
    case authorized
    case unauthorized
}

public class AppCoordinator {
  
    var window: UIWindow
    var controllerProvider: ControllerProviderProtocol

    init(window: UIWindow, provider:ControllerProviderProtocol){
        self.window = window
        self.controllerProvider = provider
    }
    
    public func start() {
        let launchState = AppLaunchState.authorized // this should be calculated by helper in big projects
        
        switch launchState {
        case .firstTime:
            print("load onboarding")
        case .authorized:
            let navigation = BaseNavigationController()
            let seriesCoordinator = controllerProvider.seriesCoordinator(navigation: navigation)
            seriesCoordinator.start()
            window.rootViewController = navigation
        case .unauthorized:
            print("load login/register")
        }
    }
}
