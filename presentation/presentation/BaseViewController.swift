//
//  BaseViewController.swift
//  presentation
//
//  Created by Rashad Shirizada on 07.11.21.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        self.view.backgroundColor = UIColor.viewBackgroundColor
    }
}
