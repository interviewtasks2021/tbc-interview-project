//
//  BaseTableController.swift
//  presentation
//
//  Created by Rashad Shirizada on 06.11.21.
//

import UIKit
import RxSwift
import RxCocoa

class BaseTableController: UITableViewController {
    var disposeBag =  DisposeBag()
    var onItem: ((Int) -> ()) = {_ in }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handleEvents()
    }
    
    private func handleEvents() {
        self.tableView.rx.itemSelected
            .subscribe(onNext: {[weak self] indexPath in
                self?.onItem(indexPath.row)
            })
            .disposed(by: disposeBag)
    }
}
